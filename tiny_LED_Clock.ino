#include "RTClib.h"
#include "MsTimer2.h"
#include "SevenSeg4.h"
#include "Wire.h"


SevenSeg4 seg(2,3,4,5,6,7,8,9, 10,11,12,13);  // seg a - g, seg dp, dig1, dig2, dig3, dig4);
RTC_DS1307 rtc;

int count = 0;
unsigned long time;


void timerintr()
{
  seg.loop();
}

void setup()
{
  seg.zeroSuppress(false);
  MsTimer2::set(5, timerintr);
  MsTimer2::start();

  Serial.begin(115200);
  Wire.begin();
  rtc.begin();

  if (! rtc.isrunning())
  {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
}

void loop()
{
  DateTime now = rtc.now();
  int disp = now.minute()*100 + now.second();
  seg.setValue(disp);

    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(' ');
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();

  delay(1000);
}
