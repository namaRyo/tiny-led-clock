#ifndef __SEVEN_SEGMENT_4_H__
#define __SEVEN_SEGMENT_4_H__

#include "Arduino.h"

class SevenSeg4
{

public:
	SevenSeg4(int a, int b, int c, int d, int e, int f, int g, int dp, int dig1, int dig2, int dig3, int dig4);

	int	setValue(int value);
	int	loop(void);
	int	zeroSuppress(boolean flag){_zero = flag;}
	int	setDp(int dp, boolean onoff){ _dp[dp] = onoff;}

private:
	int segOut(int value);
	int segOff(void);

	int _seg[8];		// segments
	boolean _dp[4];
	int _com[4];
	int _val1, _val2, _val3, _val4;
	int _dig;
	int _zero;

};
#endif	/*__SEVEN_SEGMENT_2_H__*/

