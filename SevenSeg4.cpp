#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#include <SevenSeg4.h>

const char num[10] = {
	0b00111111,
	0b00000110,
	0b01011011,
	0b01001111,
	0b01100110,
	0b01101101,
	0b01111101,
	0b00000111,
	0b01111111,
	0b01101111
};

SevenSeg4::SevenSeg4(int a, int b, int c, int d, int e, int f, int g, int dp, int dig1, int dig2, int dig3, int dig4)
{
	_seg[0] = a;
	_seg[1] = b;
	_seg[2] = c;
	_seg[3] = d;
	_seg[4] = e;
	_seg[5] = f;
	_seg[6] = g;
	_seg[7] = dp;

	_com[0] = dig1;
	_com[1] = dig2;
	_com[2] = dig3;
	_com[3] = dig4;

	_val1 = 0;
	_val2 = 0;
	_val3 = 0;
	_val4 = 0;

	_dig = 0;
	_zero = false;

	for(int i=0;i<4;i++){
		digitalWrite(_com[1], HIGH);
		pinMode(_com[i], OUTPUT);
	}

	for(int i=0;i<8;i++){
		pinMode(_seg[i], OUTPUT);
	}
}

int SevenSeg4::setValue(int value)
{
	if(value<0) value = 0 - value;
	value = value % 10000;
	_val1 = value / 1000;
	value -= _val1 * 1000;
	_val2 = value / 100;
	value -= _val2 * 100;
	_val3 = value / 10;
	_val4 = value % 10;

	return 0;
}

int SevenSeg4::loop(void)
{
	digitalWrite(_com[0], HIGH);
	digitalWrite(_com[1], HIGH);
	digitalWrite(_com[2], HIGH);
	digitalWrite(_com[3], HIGH);
	if(_dig==0){
		if((_val1==0)&&(_zero)){
			segOff();
		}else{
			segOut(_val1);
		}
		// dp
		digitalWrite(_seg[7], _dp[0] ? HIGH : LOW);
		digitalWrite(_com[0], LOW);
		_dig = 1;
	}else if(_dig==1){
		if((_val1==0)&&(_val2==0)&&(_zero)){
			segOff();
		}else{
			segOut(_val2);
		}
		// dp
		digitalWrite(_seg[7], _dp[1] ? HIGH : LOW);
		digitalWrite(_com[1], LOW);
		_dig = 2;
	}else if(_dig==2){
		if((_val1==0)&&(_val2==0)&&(_val3==0)&&(_zero)){
			segOff();
		}else{
			segOut(_val3);
		}
		// dp
		digitalWrite(_seg[7], _dp[2] ? HIGH : LOW);
		digitalWrite(_com[2], LOW);
		_dig = 3;
	}else{
		segOut(_val4);
		// dp
		digitalWrite(_seg[7], _dp[3] ? HIGH : LOW);
		digitalWrite(_com[3], LOW);
		_dig = 0;
	}
}

int SevenSeg4::segOut(int value)
{
	value = value % 10;
	char pat = num[value];
	for(int i=0;i<7;i++){
		if(pat&(1<<i)){
			digitalWrite(_seg[i], HIGH);
		}else{
			digitalWrite(_seg[i], LOW);
		}
	}
	return 0;
}

int SevenSeg4::segOff(void)
{
	for(int i=0;i<7;i++){
		digitalWrite(_seg[i], LOW);
	}
	return 0;
}
